/**
 * Object for comfortable setting of certain attributes to all Movable Objects in a stage, which belongs to this SetOfMovables Object. 
 * Firstly this object should be constructed, where all attributes are set by the input given as the parameter.
 * Then the Movable-object is assigned to this SetOfMovables-instance by using "addToMovables(movable)", where movable is the Movable-Object.
 * For more information see the code below.
 * @param {int} setOMovablesObject.steps integer value to decide how the shapes will be rotated, steps = 0 is a fluent rotation, for steps > 0 
 * the form only rotates if the finger on the form passes 360/(2*steps)-degrees by pressing and moving around its center
 * @param {int} setOMovablesObject.parameter1 parameter TODO: to decide which style of rotating the shapes will adapt
 * @param {int} setOMovablesObject.jumps factor for which the degree of rotation will be multiplied, jumps = 1 means the form 
 * follows the finger while rotating
 * @param {int} setOMovablesObject.radius_innerCircle value for setting the radius of the inner circle while using gestures on the form
 * @param {int} setOMovablesObject.factor_outerCircle value for setting the radius of the outer circle while using gestures on the form,
 * this value is multiplied with the radius of the inner circle and the result is the radius of the outer circle
 * @param {boolean} setOMovablesObject.allowToMove allowToMove = true activates functionality of gestures, allowToMove = false deactivates them
 * @param {string} setOMovablesObject.shadow_innerCircle sets the color of the shadow of the inner circle while rotating the form
 * @param {string} setOMovablesObject.color_innerCircle sets the color of the inner circle while using the gestures
 * @param {string} setOMovablesObject.color_outerCircle sets the color of the outer circle while using the gestures
 * @param {float} setOMovablesObject.alpha_innerCircle sets the alpha-value of the inner circle while using the gestures
 * @param {float} setOMovablesObject.alpha_outerCircle sets the alpha-value of the outer circle while using the gestures
 */
var SetOfMovables = function(  setOMovablesObject )
{
    // list, where all Movable-Objects will be stored
    this.setOfMovables      = new Array();
    this.steps              = setOMovablesObject.steps;
    this.jumps              = setOMovablesObject.jumps;
    this.parameter1         = setOMovablesObject.parameter1;
    this.radius_innerCircle = setOMovablesObject.radius_innerCircle; 
    this.factor_outerCircle = setOMovablesObject.factor_outerCircle;
    this.allowToMove        = setOMovablesObject.allowToMove;
    this.shadow_innerCircle = setOMovablesObject.shadow_innerCircle; 
    this.color_innerCircle  = setOMovablesObject.color_innerCircle; 
    this.color_outerCircle  = setOMovablesObject.color_outerCircle; 
    this.alpha_innerCircle  = setOMovablesObject.alpha_innerCircle;
    this.alpha_outerCircle  = setOMovablesObject.alpha_outerCircle;

    /**
     * function for adding Movable-objects to the setOfMovables-list and sets
     * several parameters as described in constructor call of SetOfMovables
     * @param {Movable} Movable Movable-object which will be added to the list and its parameters will be set
     */
    this.addToMovables  = function(movable)
    {
        this.setOfMovables.push(movable);
        if( this.steps != undefined)
            movable.rotate_in_steps(this.steps);
        if( this.parameter1 != undefined)    
            movable.setParameter1(this.parameter1);
        if( this.jumps != undefined) 
            movable.setJumps(this.jumps);
        if( this.radius_innerCircle != undefined)
            movable.setDistance(this.radius_innerCircle);
        if( this.factor_outerCircle != undefined)
            movable.setFactorOuterCircle(this.factor_outerCircle);
        if( this.allowToMove != undefined)
            movable.setAllowToMove(this.allowToMove);
        if( this.shadow_innerCircle != undefined)
            movable.setInnerCircle_shadow(this.shadow_innerCircle);
        if( this.color_innerCircle != undefined)
            movable.setColor_innerCircle(this.color_innerCircle);
        if( this.color_outerCircle != undefined)
            movable.setColor_outerCircle(this.color_outerCircle);
        if( this.alpha_innerCircle != undefined)
            movable.setAlpha_innerCircle(this.alpha_innerCircle);
        if( this.alpha_outerCircle != undefined)
            movable.setAlpha_innerCircle(this.alpha_outerCircle);

        updateSetOfMovables(this.setOfMovables);
    };

    /**
     * function to update the intern array setOfMovables after adding a movable object
     * @param {Array} list array 
     */
    function updateSetOfMovables(list)
    {
        let list_temp   = new Array();
        list_temp       = list;
        this.setOfMovables = list_temp;
    };

    /**
     * function to return the array-size of the setOfMovables-array
     */
    this.lengthSize = function()
    {
        return this.setOfMovables.length;
    };
};

/**
 * Creates new object out of a shape which will support the touch-gestures for moving, rotating and mirroring with feedback
 * @param {createjs.Shape()} shape Shape for which a copy with the mentioned gestures will be constructed or
 * this object will get the gestures. This depends on the value for the input "type": if type is undefined,
 * the shape gets the functionality of the gestures. Otherwise a copy will be created, which is directly accessible via
 * the "shape" attribut of this Movable-Object.
 * @param {int} regXCor regX value for Shape
 * @param {int} regYCor regX value for Shape
 * @param {string} type for a type otherwise than undefined (no entry), a copy of the shape will be created as mentioned
 * before, for undefined, the shape in the input of the Movable-Constructor gets the multitouch-gestures-functionality.
 */
var Movable = function(shape, regXCor, regYCor, type)
{
    this.type_form       = type;   
    let stage            = MTLG.getStageContainer();

    // TODO: parameter to decide which event listeners to use
    this.parameter1;
    this.id             = "";

    // Specifies the sizes of the screen due to adaption on screen format
    var levelOptions_1  = MTLG.getOptions();
    var height_1        = levelOptions_1.height;
    var width_1         = levelOptions_1.width;

    // Sets the radius circles for the inner- and outer circle via default values.
    // This values gets transformed later in this constructor depending on the current window format.
    let radius_circle       = 200;
    var radius_circle_m     = radius_circle;
    let factorOuterCircle   = 1.2;

    // Returns the variable "radius_circle_m"
    function getRadius_circle_m()
    {
        return radius_circle_m;
    };

    // Variable for deciding when to reflect the shape. Dist will be updated after every pressmove event.
    // dist is compared there to the modified radius of the inner circle.
    let dist;

    // creating a new object as a copy depending if input for "type" is not empty.
    var q;
    console.log("type_form"+ this.type_form);
    if(this.type_form == "Shape")
    {
        let shape_temp      = new createjs.Shape();
        shape_temp          = shape;
        this.shape          = shape_temp;
        q                   = this.shape;  
    }
    else if(this.type_form == "Container")
    {
        let shape_temp      = new createjs.Container();
        shape_temp          = shape;
        this.shape          = shape_temp;
        q                   = this.shape; 
    }
    else
    {
        // or makes the shape in input to an object with implemented gestures for moving, rotating and reflecting.
        q                   = shape;
    }

    // sets reg in x- and y coordinate for rotating and setting shape q 
    q.regX              = regXCor;
    q.regY              = regYCor;

    // fac1 is factor for width and fac2 is factor for height of actual window (due to 16/9 screen format 
    // in standard MTLG options and possible different actual format )
    this.fac1;
    this.fac2;

    // calculates fac1 and fac2 depending on actual window format
    calculateFactorsForFormat(window.innerWidth, window.innerHeight, width_1, height_1);

    // Variables for rotating:

    // Flag if form is allowed to rotate.
    this.rotable        = false;

    // this.angle is an initial value for accessing the current rotation of the form.
    // rotate_angle is used to store the current rotation-value of the form for rotating it without beginning 
    // from the start position of the form.
    this.angle          = 0;
    let rotate_angle    = this.angle;
    this.angle          = rotate_angle;

    // Default definition of the value steps. This integer-value decides how the form will be rotated:
    // steps = 0 is a fluent rotation, for steps > 0 the form only rotates if the finger on the form passes 360/(2*steps)-degrees by pressing and moving around its center
    var steps           = 0;

    // Default initialization of the value allow_reflect. If allow_reflect is true, the form is allowed to reflect.
    // Otherwise it isn´t. This happens when the form is reflected the first time during the pressmove-event on the
    // outer-circle and turns back to true, if a pressup-event occurs at the form of the inner circle.
    let allow_reflect   = true;

    // rotating_flag is a boolean flag to catch the angle of the first finger-touch on the inner-circle while rotating.
    // This is necessary to rotate the form independent from the position of the finger touch starting the rotating gesture.
    let rotating_flag   = true;

    // Value to add to the degree while rotating. This value stores the angle from the first touch-point during the rotating-
    // gesture and does not change because of changing of the value of rotating_flag
    let rotating_beginningPoint = 0; 

    // Default definition of rotation_jumps. This is a factor, which is multiplied to the angle while rotating.
    let rotation_jumps  = 1;

    // Value to add to the degree for rotating after reflecting the form.
    let reflecting_beginningPoint = 0; 

    // Flag to store the degree of the initial pressmove-event while rotating on the inner circle.
    // This value is stored in the variable rotate_start.
    let rotate_flagRotateUntilReflect = true;
    let rotate_start    =  0;

    // Boolean to decide if the form is allowed to use its implemented gestures or not.
    // true means it is allowed, false means its not.
    let allowToMove    = true;

    // Default value of the shadow appearing during the rotation gesture.
    var innerCircle_shadow = "magenta";

    // Default colors of the inner- and outer circle. 
    var color_innerCircle = createjs.Graphics.getRGB(0,0,200);
    var color_outerCircle = "yellow";

    // Default alpha-values of the inner- and the outer circle.
    var alpha_innerCircle = 0.15;
    var alpha_outerCircle = 0.2;

    // variables for the inner- and the outer circle
    var innerCircle;
    var circle_reflect;

    // constructs the inner- and the outer circle
    construct_innerCircle(color_innerCircle, radius_circle);
    construct_outerCircle(color_outerCircle, radius_circle, factorOuterCircle);

    /**
     * function to draw inner circle to interaction for rotating.
     * @param {string} color color of circle 
     * @param {int} radius radius of inner circle.
     */ 
    function construct_innerCircle(color, radius)
    {
        let g = new createjs.Graphics();
        g.beginFill(color);
        g.drawCircle(0, 0, radius);
        innerCircle = new createjs.Shape(g);
    };

    /**
     * function to draw circle to indicate whether form was reflected or not, circle appears only if form
     * was not reflected yet
     * @param {string} color color of circle 
     * @param {int} radius radius of inner circle, see function declaration above
     * @param {float} factor factor to be mutiplied with radius of inner circle, where the result is the radius
     * of the outer circle
     */
    function construct_outerCircle(color, radius, factor)
    {
        // Creates a circle shape with radius radius_circle*factor and name "circle_reflect",
        // tis circle hasn´t any event-listeners and appears only if shape is reflectable,
        // full circle if factor <=1
        if(factor <= 1)
        {
            let g_reflect = new createjs.Graphics();
            g_reflect.beginFill(color);
            g_reflect.drawCircle(0, 0, radius * factor);
            circle_reflect = new createjs.Shape(g_reflect);
        }
        else
        {
            // circle ring if factor > 1
            let temp                = radius*factor - radius;
            console.log(temp);
            circle_reflect      = new createjs.Shape();
            circle_reflect.graphics.setStrokeStyle(temp);
            circle_reflect.graphics.beginStroke(color);
            circle_reflect.graphics.drawCircle(0, 0 , radius + temp/2);
        }
    };
     
    // function for external setting of the color of the inner circle. A new circle is created here.
    this.setColor_innerCircle = function(color)
    {
        color_innerCircle = color;
        construct_innerCircle(color, radius_circle);
    };

    // function for external setting of the color of the outer circle. A new circle is created here.
    this.setColor_outerCircle = function(color)
    {
        color_outerCircle = color;
        construct_outerCircle(color, radius_circle, factorOuterCircle);
    };

    // function for external setting of the alpha value of the inner circle.
    this.setAlpha_innerCircle = function(alpha)
    {
        alpha_innerCircle = alpha;
    };

    // function for external setting of the alpha value of the outer circle.
    this.setAlpha_outerCircle = function(alpha)
    {
        alpha_outerCircle = alpha;
    };

    // function for external setting of the distance value. The value for radius_circle_m will be updated
    // and the inner- and outer circle wille be created with modified values.
    this.setDistance = function(radius)
    {
        radius_circle = radius;
        radius_circle_m = radius_circle * getFac2();
        construct_innerCircle(color_innerCircle, radius);
        construct_outerCircle(color_outerCircle, radius, factorOuterCircle);
    };

    // function for external setting of the identification number of this Movable-Object.
    this.setID       = function(id)
    {
        this.id = id;
    };

    // function for external setting of the factor, which will be multiplied with the radius of the
    // inner circle for determinde the radius of the outer circle.
    this.setFactorOuterCircle = function(factor)
    {
        factorOuterCircle = factor;
        construct_outerCircle(color_outerCircle, radius_circle, factor);
    };

    // function for external access of the form of the Movable-Object.
    this.getMovableShape = function()
    {
        return q;
    };

    // Setter of attribute "rotable"
    function setRotable(bool)
    {
        this.rotable = bool;
    };

    // Getter of attribute "rotable"
    function getRotable()
    {
        return this.rotable;
    };

    /**
     * function to calculate in which sector a specific degree belongs.
     * The circle "innerCircle" is separated imaginary in eight sectors with the same size, with a width of 22.5 degrees
     * and the first sector is located from degree 348.75 to degree 11.25 with degree 0 on top of the circle.
     * @param {int} degree value on a circle from degree 0 to 359
     * @return returns an integer from 0 to 15. It indicates in which sector the point on the circle belongs to.
     */
    function calculateInWhichSector(degree)
    {
        return ((degree - (degree % 22.5))/ 22.5)+16 %16;
    };

    /**
     * function determines which case to use for reflecting form depending on:
     * position of finger on circle "innerCircle" while leaving it and 
     * the passed distance while rotating due to last fired pressmove event.
     * The cases are described in the function named "reflect"
     * @param {int} degree_finger degree of position of finger on circle "innerCircle" while leaving it
     * @param {int} degree_rotation degree of passed distance while rotating due to first fired pressmove event during rotating-action.
     * @return returns integer 0, 1, 2 or 3
     */
    function distinctCaseForSector(degree_finger, degree_rotation)
    {
        let array_a = [15, 0, 7, 8];
        let array_b = [5, 6, 13, 14];
        let array_c = [3, 4, 11, 12];
        let array_d = [1, 2, 9, 10];

        if( (isIn(degree_finger, array_a) &&  isIn(degree_rotation, array_a)) ||  (isIn(degree_finger, array_b) &&  isIn(degree_rotation, array_b)) || (isIn(degree_finger, array_c) &&  isIn(degree_rotation, array_c)) || (isIn(degree_finger, array_d) &&  isIn(degree_rotation, array_d)) )
            {  return 0; }
        else if( (isIn(degree_finger, array_a) &&  isIn(degree_rotation, array_b)) || (isIn(degree_finger, array_b) &&  isIn(degree_rotation, array_a)) || (isIn(degree_finger, array_c) &&  isIn(degree_rotation, array_b)) || (isIn(degree_finger, array_d) &&  isIn(degree_rotation, array_c)) )
            {  return 1; }
        else if( (isIn(degree_finger, array_a) &&  isIn(degree_rotation, array_c)) || (isIn(degree_finger, array_b) &&  isIn(degree_rotation, array_d)) || (isIn(degree_finger, array_c) &&  isIn(degree_rotation, array_a)) || (isIn(degree_finger, array_d) &&  isIn(degree_rotation, array_b)) )
            {  return 2; }
        else
            {  return 3; }
    };

    /**
     * function to determine if an element is contained in an array
     * @param {object} element element to be checked
     * @param {Array} array array to be examined
     * @return true if element is in array, otherwise false
     */
    function isIn(element, array)
    {
        let temp = false;
        for(let i= 0; i<array.length; i++)
        {
            if(element == array[i])
                temp = true;
        }
        return temp;
    };

    /**
     * function to "reflect" a shape by setting the skewX, skewY and/or rotation value of the shape
     * depending on the position, the finger on the circle left the circle due to pressmove event and 
     * depending on the distance, the finger on the circle passed due rotating while the pressmove event
     * on the circle.
     * The passed distance while rotating is necessary because during rotating without moving the form,
     * no pressmove event of it is firing.
     * @param {int} angle_temp1 degree which describes the position, the finger on the circle leaves the circle
     * to trigger the reflect function. Degree 0 is located on top of the circle, rises in clock-wise rotation
     * and reaches a max-value of 359.
     */
    function reflect(angle_temp1)
    {
        // decides in which sector the degree for the leaving finger and the passed distance while rotating
        // belongs
        let temp_sector_leaveCircle     = calculateInWhichSector(angle_temp1);
        let temp_rotate                 = calculateInWhichSector(rotate_start);
        let temp_sector_rotatedDistance = temp_sector_leaveCircle - temp_rotate;
        // calculates integer values of the previous degree values
        temp_sector_leaveCircle         = parseInt(temp_sector_leaveCircle.toFixed(0),10);
        temp_sector_rotatedDistance     = parseInt(temp_sector_rotatedDistance.toFixed(0),10);
        // converting to a positive value
        if(temp_sector_leaveCircle < 0)
            temp_sector_leaveCircle    += 16;
        if(temp_sector_rotatedDistance < 0)
            temp_sector_rotatedDistance+= 16;
        // determines the case, how the shape will be transformed depending on calculated values
        let case_sector                 = distinctCaseForSector(temp_sector_leaveCircle, temp_sector_rotatedDistance);

        if( case_sector == 0)
        {
            // case for reflecting horizontally
            q.skewX = (q.skewX +180) %360;
        }
        else if( case_sector == 2)
        {
            // case for reflecting vertically
            q.skewY = (q.skewY +180) %360;
        }
        else if( case_sector == 1)
        {
            // case for reflecting diagonally, with line for reflecting from upper left to down right side
            update_reflecting_beginningPoint(90);
            q.rotation += 90;
            q.skewY = (q.skewY + 180) %360;  
        }
        else if( case_sector == 3)
        {
            // case for reflecting diagonally, with line for reflecting from upper right to down left side
            update_reflecting_beginningPoint(-90);
            q.rotation -= 90;
            q.skewY = (q.skewY + 180) %360;
        }     
    };

    /**
     * function sets the variable allow_reflect on bool
     * @param {boolean} bool boolean value which the variable allow_reflect is set on
     */
    function setAllow_reflect(bool)
    {
        allow_reflect = bool;
    };

    /**
     * function returns the current value of the variable allow_reflect
     * @return boolean
     */
    function getAllow_reflect()
    {
        return allow_reflect;
    };

    /**
     * function to get the value of the allowToMove variable
     */
    function getAllowToMove()
    {
        return allowToMove;
    };

    /**
     * function for external use to set the variable allowToMove of this Movable object by the parameter bool.
     * Is used to forbid functionality of implemented event features in Movable instance to prevent intersections
     * with own events.
     */
    this.setAllowToMove = function(bool)
    {
        setAllowToMove(bool);
    };

    // function for external setting of the shadow-attribut of the inner circle by coloring it with the input "color"
    this.setInnerCircle_shadow = function(color)
    {
        innerCircle_shadow = color;
    };

    /**
     * function for external setting of coordinates of Movable object to specified x- and y coordinate
     */
    this.moveFormTo = function(x, y)
    {
        innerCircle.set(stage.globalToLocal(x, y));
        q.set(stage.globalToLocal(x, y));
    };

    /**
     * function for external setting of the rotation of the form by a degree calculated depending on the input "modulus".
     * The value "360" is devided into "360/modulus" parts and the forms rotation is set to its rotation value 
     * minus or plus the part, where the result % modulus results in 0. 
     */
    this.setExternalRotation = function(modulus)
    {
        let degree = q.rotation;
        let alpha = ((degree + modulus) + 360) % modulus;
        if( Math.abs(alpha) < modulus/2 )
            q.rotation = Math.round((q.rotation + 360 - alpha) % 360);  
        else
            q.rotation = Math.round((((q.rotation + 360 + (modulus - alpha) % modulus)) % 360));
    };

    /**
     * function for internal setting of the variable allowToMove by the parameter bool
     * @param {Boolean} bool 
     */
    function setAllowToMove(bool)
    {
        allowToMove = bool;
    };

    // Getter of private attribute "fac1"
    function getFac1()
    {
       return Number.parseFloat(this.fac1).toPrecision(3);
    };

    // Getter of private attribute "fac2"
    function getFac2()
    {
        return Number.parseFloat(this.fac2).toPrecision(3);
    };

    /**
     * function to set parameter1 value of this Movable object to value parameter1 by functions outside
     * @param {int} parameter1 value to set parameter1 value of this Movable object
     */
    this.setParameter1 = function(parameter1)
    {
        setParam1(parameter1);
    };

    // function to set value of variable "parameter1" to input "parameter1"
    function setParam1(parameter1)
    {
        this.parameter1 = parameter1;
    };

    // function to set attribute "steps"
    this.rotate_in_steps = function(steps)
    {
        setSteps(steps);
    };

    // function to set attribute "rotation_jumps"
    this.setJumps = function(jumps)
    {
        rotation_jumps = jumps;
    };

    /**
     * function to distinct how the form will be rotated
     * @param {int} angle value which should be converted to its configured pendant.
     * The form only rotates if the finger on the inner circle passes 360/(2*steps)-degrees.
     * @return returns modified angle value
     */
    function make_steps(angle)
    {
        if(getSteps() == 0)
        {
            // returns the angle if value for angle is 0
            return rotation_jumps * angle;
        }
        else
        {
            // if value for angle is not 0, firstly the circle is separated into angle*2 sections
            // and it returns the angle value modulus this sections
            let i           = 360 / (2*getSteps());
            let angle_mod   = angle - (angle % i);
            return rotation_jumps * angle_mod;
        }
    };

    // returns variable "steps" of this Movable object
    function getSteps()
    {
        return steps;
    };

    /**
     * function to set steps value of this Movable object to value steps_value
     * @param {int} steps_value value to set steps value of this Movable object
     */
    function setSteps(steps_value)
    {
        steps = steps_value;
    };

    /**
     * function to update the variable rotating_beginningPoint, important to let the pentomino stay in
     * its position when the finger is lifted from the circle while rotating.
     * @param {int} degree degree of the beginning touch on the circle t while rotating. This value will be
     * subtracted when calculating the degree of rotating the shape.
     */
    function update_rotating_beginningPoint(degree)
    {
        if(rotating_flag)
            rotating_beginningPoint = degree;
    };

    /**
     * function to update the variable reflecting_beginningPoint, important to let the pentomino stay in
     * its position when the finger is lifted from the circle while reflecting the shape. 
     * @param {int} degree degree of the area where the touch on the circle t left it. This value will be
     * subtracted when calculating the degree of rotating the shape.
     */
    function update_reflecting_beginningPoint(degree)
    {
        reflecting_beginningPoint = degree;
    };

    /**
     * function sets the boolean rotating_flag on the boolean parameter bool. This function is necessary to
     * set the degree in the function update_rotating_beginningPoint by the first touch on the circle t 
     * while rotating and should forbid the change of this value to a pressup event.
     * @param {Boolean} bool Boolean value to set the rotating_flag variable
     */
    function set_rotating_flag(bool)
    {
        rotating_flag = bool;
    };

    /**
     * function: There is a problem of setting the skew value after the user used rotating operations
     * on the form. There it sets the skew value depending on the rotating value before the rotating operations after the first one.
     * This function supplies the function update_rotate_start by setting the variable rotate_flagRotateUntilReflect
     * on the parameter bool. Is used at the beginning of the rotating process and in pressup event.
     * @param {Boolean} bool 
     */
    function set_rotate_flagRotateUntilReflect(bool)
    {
        rotate_flagRotateUntilReflect = bool;
    };

    /**
     * function sets the variable rotate_start on parameter value. This function is used to support the
     * correct setting of the skew attributes of the shape for reflecting operation.
     * @param {int} value degree value of touch on circle innerCircle. Should be set at the beginning of the rotating 
     * operation and is supplied by the function set_rotate_flagRotateUntilReflect.
     */
    function update_rotate_start(value)
    {
        rotate_start = value;
    };

    /**
     * function returns true if the shape has the same orientation as in the beginning of rendering the stage
     * or false, if orientation is inverted (upside down or reflected from left to right).
     */
    function getOrientation()
    {
        return ((q.skewX%360) == 0 && (q.skewY%360 == 0)) || ((q.skewX%360 == 180) && (q.skewY%360 == 180));
    };

    // initial set of variable this.rotable on false
    setRotable(false);

    // Event Listener for pressmove event for handling Multi-Touch operations on the form.
    q.on("pressmove", rotateByTouch2);

    /**
     * function to handle the pressmove event in the event listener above. In this function, the pressmove
     * event is considered as an event fired by touch gestures on a Multi-Touch display.
     * @param {Event} evt pressmove event for form q
     */
    function rotateByTouch2(evt)
    {
        if(getAllowToMove() == true)
        {
            // before shape get reflected, a yellow circle surrounds it as a mark for possible reflecting
            // and appears only if reflecting is possible
            if(getAllow_reflect())
                stage.addChild(circle_reflect), circle_reflect.alpha = alpha_outerCircle,
                circle_reflect.set(stage.globalToLocal(evt.stageX, evt.stageY));
            // adds circle t with hardcoded alpha value to the stage
            innerCircle.alpha = alpha_innerCircle;
            stage.addChild(innerCircle);
            setRotable(true);
            // circle and shape will be set on coordinates delivered by the fired event
            innerCircle.set(stage.globalToLocal(evt.stageX, evt.stageY));
            q.set(stage.globalToLocal(evt.stageX, evt.stageY));

            // event listeners which provides the rotating-, and the reflecting operations
            innerCircle.on("pressmove", pressmoveForCircleByTouch2);
            innerCircle.on("pressup", pressupForCircleByTouch);
            q.on("pressup", pressupForShapeByTouch);
        }
    };

    /**
     * function to handle the pressmove event for circle innerCircle in the event listener above. In this function,
     * the function to decide if the form will be rotated or reflected, is activated and the appearing shadow of
     * the inner circle is called here.
     * @param {Event} evt2 pressmove event for circle innerCircle
     */
    function pressmoveForCircleByTouch2(evt2)
    {
        innerCircle.shadow = new createjs.Shadow(innerCircle_shadow, 0, 0, 30);
        if(evt2.stageX != q.x && evt2.stageY != q.y && getRotable())
            decideRotateOrReflect(q.x, q.y, evt2.stageX, evt2.stageY);
    };

    /**
     * function to handle pressup event for circle innerCircle. Several variables will be set on default and 
     * rotation of shape will be set on new rotation value.
     * @param {Event} evt pressup event for circle
     */
    function pressupForCircleByTouch(evt)
    {
        rotate_angle = q.rotation %360;
        innerCircle.shadow = new createjs.Shadow("green", 0, 0, 0);
        setAllow_reflect(true);
        stage.addChild(circle_reflect), circle_reflect.alpha = alpha_outerCircle,
        circle_reflect.set(stage.globalToLocal((q.x)*getFac1(), (q.y)*getFac2()));
        set_rotating_flag(true);
        rotating_beginningPoint = 0;
        update_reflecting_beginningPoint(0);
        set_rotate_flagRotateUntilReflect(true);
        update_rotate_start(0);
    };

    /**
     * function to handle pressup event for shape q. Several variables will be set on default.
     * @param {Event} evt pressup event for shape
     */
    function pressupForShapeByTouch(evt)
    {
        innerCircle.shadow = new createjs.Shadow("green", 0, 0, 0);
        stage.removeChild(circle_reflect);
        stage.removeChild(innerCircle);
        setRotable(false);
        setAllow_reflect(true);
        update_reflecting_beginningPoint(0);
        set_rotate_flagRotateUntilReflect(true);
        update_rotate_start(0);
    };

    // Event Listener for pressmove event for handling keyboard- and mouse operations.
    document.addEventListener("keypress", rotateByKeypress);

    /**
     * function to handle the pressmove event in the event listener above. In this function, the pressmove
     * event is considered as an event fired by drag and drop by mouse and pressing the key "e" on the keyboard.
     * @param {Event} event pressmove event for shape q
     */
    function rotateByKeypress(event)
    {
        if(getAllowToMove() == true)
        {
            if(event.keyCode == "101")
            {
                // before shape get reflected, a yellow circle surrounds it as a mark for possible reflecting
                // and appears only if reflecting is possible
                if(getAllow_reflect())
                    stage.addChild(circle_reflect), circle_reflect.alpha = alpha_outerCircle,
                    circle_reflect.set(stage.globalToLocal((q.x)*getFac1(), (q.y)*getFac2()));
                else   
                    stage.removeChild(circle_reflect);
                // adds circle t with hardcoded alpha value to the stage in position of the shape q
                innerCircle.alpha = alpha_innerCircle;
                stage.addChild(innerCircle);
                setRotable(true);
                innerCircle.set(stage.globalToLocal((q.x)*getFac1(), (q.y)*getFac2()));

                // event listeners which provides the rotating-, and the reflecting operations
                innerCircle.on("pressmove", pressmoveForCircleByKeypress);
                innerCircle.on("pressup", pressupForCircleByKeypress);
                document.addEventListener("keyup", keyupForDocumentByKeypress);
            }
        }
    };

    /**
     * function to handle the pressmove event for circle t in the event listener above. This function
     * decides if the shape will be rotated or reflected.
     * @param {Event} evt2 pressmove event for circle t
     */
    function pressmoveForCircleByKeypress(evt2)
    {
        innerCircle.shadow = new createjs.Shadow(innerCircle_shadow, 0, 0, 30);
        if(evt2.stageX != q.x && evt2.stageY != q.y && getRotable())
            decideRotateOrReflect(q.x, q.y, evt2.stageX, evt2.stageY);
    };

    /**
     * function to handle pressup event for circle innerCircle. Several variables will be set on default and 
     * rotation of shape will be set on new rotation value.
     * @param {Event} evt pressup event for circle
     */
    function pressupForCircleByKeypress(evt)
    {
        innerCircle.shadow = new createjs.Shadow("green", 0, 0, 0);
        rotate_angle = q.rotation %360;
        stage.removeChild(circle_reflect);
        set_rotating_flag(true);
        update_reflecting_beginningPoint(0);
        set_rotate_flagRotateUntilReflect(true);
        update_rotate_start(0);
    };

    /**
     * function to handle keyup event. Several variables will be set on default.
     * @param {Event} evt pressup event for shape
     */
    function keyupForDocumentByKeypress(evt)
    {
        innerCircle.shadow = new createjs.Shadow("green", 0, 0, 0);
        stage.removeChild(circle_reflect);
        set_rotating_flag(true);
        stage.removeChild(innerCircle);
        setRotable(false);
        setAllow_reflect(true);
        update_reflecting_beginningPoint(0);
        set_rotate_flagRotateUntilReflect(true);
        update_rotate_start(0);
    };

    /**
     * function to run the reflecting operation correctly:
     * if the shape is reflected once until next pressup event, the shape should not be reflected anymore.
     * @param {int} angle_temp1 degree, which is calculated when finger leaves circle t
     */
    function reflectShape(angle_temp1)
    {
        reflect(angle_temp1 * rotation_jumps);
        setAllow_reflect(false);
        q.rotation = q.rotation %360
    };

    /**
     * function rotates a shape depending on coordinates of shape and a second touch-point on circle t
     * @param {int} angle_temp1 calculated angle, from previous step
     */
    function rotateShape(angle_temp1)
    {
        let angle_temp2;    // rotating value after adding, subtracting and calculate modulus of dpenedent variables
        let angle1;         // actual rotation value, independend from starting position of finger for rotating

        update_rotating_beginningPoint(angle_temp1);
        set_rotating_flag(false);
        if(getOrientation())
            angle_temp2 = make_steps(angle_temp1 - rotating_beginningPoint + reflecting_beginningPoint);
        else
        {
            angle_temp2 = -make_steps(angle_temp1 - rotating_beginningPoint + reflecting_beginningPoint);            
        }
        angle1          =  angle_temp2 + rotate_angle;
        q.rotation = angle1 % 360;
    };

    /**
     * function decides if next operation is rotating or reflecting the shape, depending on variable dist.
     * @param {int} x1 x-coordinate of shape q
     * @param {int} y1 y-coordinate of shape q
     * @param {int} x2 x-coordinate of event
     * @param {int} y2 y-coordinate of event
     */
    function decideRotateOrReflect(x1, y1, x2, y2)
    {
        let angle_temp1;
        let x           = x1*getFac1();
        let y           = y1*getFac2();

        angle_temp1     = angleCalculation_inDegrees(x, y, x2, y2);
        if(rotate_flagRotateUntilReflect == true)
        {
            update_rotate_start(angle_temp1);
            set_rotate_flagRotateUntilReflect(false);
        }
        if(dist < getRadius_circle_m() || !getAllow_reflect())
        {
            rotateShape(angle_temp1);
        }
        else
        {
            stage.removeChild(circle_reflect);
            reflectShape(angle_temp1);
        }    
    };

    // MTLG standard window format is 16/9, handle possible different window formats and 
    // modifies the radius of the inner- and outer circle depending on this format:
    function calculateFactorsForFormat(innerWidth, innerHeight, width_1, height_1)
    {
        this.fac1 = innerWidth/width_1;
        this.fac2 = innerHeight/height_1;
        if(innerWidth/innerHeight > 16/9)
        {
            this.fac1 = this.fac2;
            radius_circle_m *= this.fac2;
        }
        else if(innerWidth/innerHeight < 16/9)
        {
            this.fac2 = this.fac1;
            radius_circle_m *= this.fac2;
        }
        else
        {
            radius_circle_m *= this.fac2;
        }
    };

    /**
     * function calculates the angle of point (x2,y2) on a circle with center-coordinates (x1,y1) from 0 up to 360 degrees,
     * where degree 0 is located at the top of this circle.
     * @param {int} x1 x-coordinate of center point
     * @param {int} y1 y-coordinate of center point
     * @param {int} x2 x-coordinate of second point
     * @param {int} y2 y-coordinate of second point
     */
    var angleCalculation_inDegrees = function(x1,y1,x2,y2)
    {
        let alpha   = angleCalculation_toNinetyDegrees(x1,y1,x2,y2);
        if((x1>=x2) && (y1>y2))
        {
            return (90 - alpha) + 270;
        }
        else if((x1>x2) && (y1<=y2))
        {
            return alpha + 180;
        }
        else if((x1<=x2) && (y1<y2))
        {
            return (90 - alpha) + 90;
        }
        else
        {
            return (alpha );
        }
    };

    /**
     * function calculates the angle of point (x2,y2) on a circle with center-coordinates (x1,y1) from 0 up to 90 degrees,
     * where degree 0 is located at the top of this circle. The degree from 0 to 359 on the circle is calculated
     * with "% 90".
     * @param {int} x1 x-coordinate of center point
     * @param {int} y1 y-coordinate of center point
     * @param {int} x2 x-coordinate of second point
     * @param {int} y2 y-coordinate of second point
     */
    function angleCalculation_toNinetyDegrees(x1, y1, x2, y2) 
    {
        let deltaX  = Math.abs(x2 - x1);
        let hyp     = distanceBetweenPoints(x1, y1, x2, y2);
        let alpha   = Math.asin(deltaX / hyp);

        alpha       = alpha / Math.PI * 180;

        return (alpha );
    };

    /**
     * function calculates the distance between points (x1,y1) and (x2,y2)
     */
    function distanceBetweenPoints(x1, y1, x2, y2) 
    {
        // Pythagorean theorem
        let deltaX  = Math.abs(x1 - x2);
        let deltaY  = Math.abs(y1 - y2);
        dist        = Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2));
        return dist;
    };
};

Movable.prototype = Object.create(Movable.prototype);
Movable.prototype.constructor = Movable;
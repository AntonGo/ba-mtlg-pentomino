function firstlevel_init(gameState){
  // initialize level 1

  //window.location.reload(true);
  console.log("window: "+ window);

  console.log("This players are logged in:");
  for (var i = 0; i < MTLG.getPlayerNumber(); i++) {
    console.log(MTLG.getPlayer(i));
  }

  console.log("Thie are the available game options:");
  console.log(MTLG.getOptions());

  // esit the game.settings.js
  console.log("Thie are the available game settings:");
  console.log(MTLG.getSettings());

  drawLevel1(gameState);
}

// check wether level 1 is choosen or not
function checkLevel1(gameState)
{
  if(gameState && gameState.nextLevel && gameState.nextLevel == "level1")
  {
    return 1;
  }
  return 0;
}


function drawLevel1(gameState){
  var stage = MTLG.getStageContainer();

  // sets the background
  let settingBackgroundPicture = MTLG.assets.getBitmap('img/wood1.jpg');
  settingBackgroundPicture.alpha = 0.6;
  settingBackgroundPicture.scaleX = 1.5;
  settingBackgroundPicture.scaleY = 1.5;
  stage.addChild(settingBackgroundPicture);

  console.log("Level 1 started.");
  MTLG.setBackgroundColor('white');

  // sets some input-parameters for the initialize-function in creating a level for pentomino:
  // coded field, which is needed to draw the Pentomino-grid
  // array of pentomino-bricks for using in the level (note: if you want to use two same Pentomino-bricks in a
  // level, you must modify the function "initialize", because both bricks would have the same id, the id of the
  // brick is given in a json file, where its coded structure is coded, too) 
  var pentomino_grid1 = require('../json/pentomino_grid_3x20.json');
  var pentominos = new Array(
    "f"
    , "i",
    "l","p",
    "n","t"
    ,"u","v","w","x","y","z"
  );

  // initialization of the date-object, which is used to show the passed time at the win-window 
  var seconds;
  var minutes;
  var hours;
  setInitialDateObject();

  // initialization of the actual level, depending on the style of the rotation given from the level selection widow
  var initializationInstance;
  if(gameState.formOfRotation == "fluid")
    initializationInstance = new initialize(pentominos, 0, 2, pentomino_grid1, 60, MTLG.getOptions().width/2 -(10*60), MTLG.getOptions().height/2 -(1.5*60), 1);
  else
    initializationInstance = new initialize(pentominos, 4, 2, pentomino_grid1, 60, MTLG.getOptions().width/2 -(10*60), MTLG.getOptions().height/2 -(1.5*60), 2);

  // adds the menu buttons for returning back to the level selection window
  addMenuButton();

  // event-listener to handle the event with the keyword "win-condition"
  document.addEventListener("win-condition", levelFinished);
  
  /**
   * function to add the win-messages after winning the game
   * @param {Event} evt event which was fired by this function
   */
  function levelFinished(evt) 
  {
    addWinWindow(MTLG.getOptions().width/2 - (1000/2), MTLG.getOptions().height/2 + 200, 1000, 250, getTimeDifference());
  };

  /**
   * function to switch to the level selection window after winning the level and submit to enter the level selection,
   * or leaving the game.
   * @param {boolean} bool indicates if the level was finished (with true) or leaved (with false)
   */
  async function solved(bool) 
  {
    // removes all event-listeners from the pentominos and movables. Constructed Objects stay in the session
    // in the browser, so this objects would stay in the next level even though they are not visible.
    initializationInstance.removeAllEventListenersFromMovable();
    if( gameState.arraySolvedGrids == undefined )
    {
      // default behaviour if gameState.arraySolvedGrids does not already exists
      let array = new Array();
      array.push(["level1", bool]);
      MTLG.lc.levelFinished( Object.assign(gameState, {
        nextLevel: "levelSelection",
        arraySolvedGrids: array
      }));
    }
    else
    {
      let array = gameState.arraySolvedGrids;
      array.push(["level1", bool]);
      MTLG.lc.levelFinished( Object.assign(gameState, {
        nextLevel: "levelSelection",
        arraySolvedGrids: array
      }));
    }
  }

  // initialization of the date-object
  function setInitialDateObject()
  {
    let dateObject = new Date();
    seconds   = dateObject.getUTCSeconds();
    minutes   = dateObject.getUTCMinutes();
    hours     = dateObject.getUTCHours();
  };

  // calculates the passed time from beginning of the level up to its completion in a useful interpretation,
  // in hours, minutes and seconds.
  function getTimeDifference()
  {
    let dateObject2  = new Date();
    let secondsEnd   = dateObject2.getUTCSeconds();
    let minutesEnd   = dateObject2.getUTCMinutes();
    let hoursEnd     = dateObject2.getUTCHours();

    let secondsTransfer = 0;
    if(secondsEnd - seconds < 0 ) 
      secondsTransfer++;
    
    let minutesTransfer = 0;
    if(minutesEnd - minutes < 0 ) 
      minutesTransfer++;

    let secondsRes     = ((secondsEnd - seconds) + 60) % 60;
    let minutesRes    = ((minutesEnd - secondsTransfer - minutes) + 60) % 60;
    let hoursRes      = ((hoursEnd - minutesTransfer - hours) + 60) % 60;

    let resSec, resMin, resHou;
    if(secondsRes < 10)
      resSec = "0"+secondsRes;
    else
      resSec = ""+secondsRes;
    if(minutesRes < 10)
      resMin = "0"+minutesRes;
    else
      resMin = ""+minutesRes;
    if(hoursRes < 10)
      resHou = "0"+hoursRes;
    else
      resHou = ""+hoursRes;
    
    res = ":   "+ resHou +" : "+ resMin +" : " + resSec;

    return res;
  };

  /**
   * function to remove the Movable-Objects of the level from it and add the win message windows for completing the level. 
   * In this windows, the passed time, images of a tick and a clock and an arrow with an event-listener appear.
   * @param {int} x x-coordinate of the window
   * @param {int} y y-coordinate of the window
   * @param {int} width width of the window
   * @param {int} height height of the window
   * @param {string} text text to be displayed in the windows, here its chosen as the passed time from starting the level
   */
  function addWinWindow(x, y, width, height, text)
  {
    // adding symbols for the win-window
    let spriteTick = new createjs.Bitmap("/img/check.png");
    let spriteStopwatch = new createjs.Bitmap("/img/stopwatch.png");
    spriteTick.scaleX       = 0.1; spriteTick.scaleY      = 0.1;
    spriteStopwatch.scaleX  = 0.1; spriteStopwatch.scaleY = 0.1;

    // adding resultbox for the win-window
    let resultBox = new createjs.Shape();
    resultBox.graphics.setStrokeStyle(1);
    resultBox.graphics.beginStroke("#000000");
    resultBox.graphics.beginFill("white");
    
    // adding text to the win-windows
    let textBox          = new createjs.Text( text, "60px Arial", "black");
    textBox.textBaseline = "alphabetic";

    // adding a green arrow to the win-windows
    let arrow_sprite = new createjs.Bitmap("/img/next-arrow_green_alt.png");
    arrow_sprite.scaleX = 0.3;
    arrow_sprite.scaleY = 0.3;
    arrow_sprite.shadow = new createjs.Shadow("blue", 0, 0, 10);

    // creating the win-windows and adding its content to it.
    let winMessageContainer = new createjs.Container();
    winMessageContainer.x = x;    winMessageContainer.y   = y;
    resultBox.graphics.drawRoundRectComplex( 0, 0,  width,  height,  20, 20, 20, 20 );
    winMessageContainer.addChild( resultBox, spriteTick, spriteStopwatch, textBox, arrow_sprite );
    spriteTick.x          = 100;  spriteTick.y            = 50;
    spriteStopwatch.x     = 250;  spriteStopwatch.y       = 50;
    textBox.x             = 400;  textBox.y               = 130;
    arrow_sprite.x        = 800;  arrow_sprite.y          = 50;

    // puts a second win-window upside down on the screen for players at the other side of the display
    let secondWinMessageContainer = winMessageContainer.clone(true);
    secondWinMessageContainer.x = winMessageContainer.x + width;
    secondWinMessageContainer.y = 100 + height;
    secondWinMessageContainer.rotation     = 180;

    stage.addChild(winMessageContainer, secondWinMessageContainer);

    // event-listener for the green arrow.By clicking on it, the player submits to enter the level-selection window.
    // Here, the Movable-Objects of the level are removed from it.
    winMessageContainer.addEventListener("click", function(evt)
    {
      initializationInstance.clearListMovables();
      MTLG.clearStage();
      solved(true);
    });

    // event-listener for the green arrow.By clicking on it, the player submits to enter the level-selection window.
    // Here, the Movable-Objects of the level are removed from it.
    secondWinMessageContainer.addEventListener("click", function(evt)
    {
      initializationInstance.clearListMovables();
      MTLG.clearStage();
      solved(true);
    });
  };

  // function to add the buttons for returning to the level-selection window without completing the current level.
  // The Movable-Objects will be removed from the level and all cells will be occupied by value "-1".
  function addMenuButton()
  {
    // sets the buttons for returning to the level selection
    let options_x = MTLG.getOptions().width/16;
    let options_y = MTLG.getOptions().height/9;

    let menuButtonImg = new createjs.Bitmap("/img/next-arrow_white.png");
      menuButtonImg.scaleX    = 0.2; menuButtonImg.scaleY   = 0.2;
    let menuButtonCont = new createjs.Container();
      menuButtonCont.regX = 50;     menuButtonCont.regY = 50;
      menuButtonCont.x        = options_x*15.5;   menuButtonCont.y       = options_y*0.5;
      menuButtonCont.rotation = 180;
      menuButtonCont.addChild( menuButtonImg );

    let secondMenuButtonCont = menuButtonCont.clone(true);
      secondMenuButtonCont.x = options_x * 0.5;
      secondMenuButtonCont.y = options_y * 8.5;
      menuButtonCont.rotation += 180;

    // event-listener of the button for the click-event. All relevant Objects will be deleted or their
    // functionality gets removed. Furthermore, the level will be exit.
    menuButtonCont.addEventListener("click", function(evt)
    {
      initializationInstance.clearListMovables();
      initializationInstance.deleteFreeCellsAfterAbortion();
      MTLG.clearStage();
      solved(false);
    });

    // event-listener of the button for the click-event. All relevant Objects will be deleted or their
    // functionality gets removed. Furthermore, the level will be exit.
    secondMenuButtonCont.addEventListener("click", function(evt)
    {
      initializationInstance.clearListMovables();
      initializationInstance.deleteFreeCellsAfterAbortion();
      MTLG.clearStage();
      solved(false);
    });
    stage.addChild( menuButtonCont, secondMenuButtonCont );
  };

}
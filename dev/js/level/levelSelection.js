function levelSelection_init(gameState){
  // initialize level 1

  //window.location.reload(true);
  console.log("window: "+ window);

  console.log("This players are logged in:");
  for (var i = 0; i < MTLG.getPlayerNumber(); i++) {
    console.log(MTLG.getPlayer(i));
  }

  console.log("Thie are the available game options:");
  console.log(MTLG.getOptions());

  // esit the game.settings.js
  console.log("Thie are the available game settings:");
  console.log(MTLG.getSettings());

  drawLevelSelection(gameState);
}

// check wether level 1 is choosen or not
function checkLevelSelection(gameState)
{
  if(gameState && gameState.nextLevel && gameState.nextLevel == "levelSelection")
  {
    return 1;
  }
  return 0;
}


function drawLevelSelection(gameState){
  // log in demo player if not yet present
  if (!MTLG.getPlayerNumber()) {
    console.log("Logging in");
    MTLG.lc.goToLogin(); //Leave Main Menu and go to login
    }

    // get stage to add objects
    var stage = MTLG.getStageContainer(); 

    // variable to later distinction which form of rotation to use
    var formOfRotation;

    // sets gameState.formOfRotation to the default value "fluid" if players enter the game, or
    // on the set value.
    if(gameState.formOfRotation == undefined || gameState.formOfRotation == "fluid")
        formOfRotation = "fluid";
    else
        formOfRotation = "90_degrees";

    // setting of the background
    MTLG.setBackgroundColor('white');
    let settingBackgroundPicture = MTLG.assets.getBitmap('img/wood-fibre-boards.jpg');
    settingBackgroundPicture.scaleX = 1.5;
    settingBackgroundPicture.scaleY = 1.5;
    stage.addChild(settingBackgroundPicture);

    // setting of the buttons for the selection of the level. Width and Height of te buttons are chosen 
    // uniformly as the max-value, depending of the grids as the labels.
    var buttonWidth       = 0;
    var buttonHeight      = 0;
    var buttonIconLevel1 = new Pentomino_layerGrid(20, require('../json/pentomino_grid_3x20.json').cells, 0, 0, true);
    if( buttonIconLevel1.width > buttonWidth)
        buttonWidth = buttonIconLevel1.width;
    if( buttonIconLevel1.height > buttonHeight)
        buttonHeight = buttonIconLevel1.height;
    var buttonIconLevel2 = new Pentomino_layerGrid(20, require('../json/pentomino_grid_bigZ.json').cells, 0, 0, true);
    if( buttonIconLevel2.width > buttonWidth)
        buttonWidth = buttonIconLevel2.width;
    if( buttonIconLevel2.height > buttonHeight)
        buttonHeight = buttonIconLevel2.height;

    var levelButton1 = drawLevelButton(buttonWidth, buttonHeight, buttonIconLevel1, MTLG.getOptions().width / 3 - buttonWidth, MTLG.getOptions().height / 2 - buttonHeight/2, "level1");
    var levelButton2 = drawLevelButton(buttonWidth, buttonHeight, buttonIconLevel2, MTLG.getOptions().width*2 / 3 - buttonWidth, MTLG.getOptions().height / 2 - buttonHeight/2, "level2");

    
    // add objects to stage
    stage.addChild(levelButton1, levelButton2);

    // if a stage is solved correctly, a tick will be placed on the buttons for the level selection.
    // This is made separately for each level.
    if(!(gameState.arraySolvedGrids == undefined))
    {
        console.log("gameState.arraySolvedGrids: "+ gameState.arraySolvedGrids);
        for(let i=0; i<gameState.arraySolvedGrids.length; i++)
        {
            console.log( "" + gameState.arraySolvedGrids[i][0] + " " + gameState.arraySolvedGrids[i][1]);
            if(gameState.arraySolvedGrids[i][0] == "level1" && gameState.arraySolvedGrids[i][1] == true)
            {
                let spriteTick      = new createjs.Bitmap("/img/check.png");
                spriteTick.scaleX   = 0.1;  
                spriteTick.scaleY   = 0.1;
                spriteTick.x        = levelButton1.x + buttonWidth - 50;  
                spriteTick.y        = levelButton1.y - 50; 
                console.log(gameState.arraySolvedGrids);
                stage.addChild(spriteTick);
            }
            if(gameState.arraySolvedGrids[i][0] == "level2" && gameState.arraySolvedGrids[i][1] == true)
            {
                let spriteTick      = new createjs.Bitmap("/img/check.png");
                spriteTick.scaleX   = 0.1;  
                spriteTick.scaleY   = 0.1;
                spriteTick.x        = levelButton2.x + buttonWidth - 50;
                spriteTick.y        = levelButton2.y - 50;
                stage.addChild(spriteTick);
            }
        }
    }

    // creating buttons for changing style of rotation
    var buttonContainer_fluentRotation  = rotationButton(MTLG.getOptions().width*7/8, MTLG.getOptions().height*3/8, "/img/arrow-fluent1.png", 70, 50, -20);
    var buttonContainer_90_degrees      = rotationButton(MTLG.getOptions().width*7/8, MTLG.getOptions().height*5/8, "/img/arrow1.png", 135, 75, 35);

    // event-listener for buttons for updating the style of rotation to fluent-rotation for the next 
    // level to play, listener is called when the button is clicked.
    buttonContainer_fluentRotation.addEventListener("click", function(evt)
    {
        formOfRotation  = "fluid";
        buttonContainer_fluentRotation.shadow       = new createjs.Shadow("green", 0, 0, 20);
        buttonContainer_90_degrees.shadow           = new createjs.Shadow("green", 0, 0, 0);
        console.log("Rotating set to fluid");
    });

    // event-listener for buttons for updating the style of rotation to 90-degree-rotation for the next 
    // level to play, listener is called when the button is clicked.
    buttonContainer_90_degrees.addEventListener("click", function(evt)
    {
        formOfRotation = "90_degrees";
        console.log("Rotating set to 90-degrees");
        buttonContainer_90_degrees.shadow           = new createjs.Shadow("green", 0, 0, 20);
        buttonContainer_fluentRotation.shadow       = new createjs.Shadow("green", 0, 0, 0);
    });

    stage.addChild(buttonContainer_90_degrees, buttonContainer_fluentRotation);

    // sets shadow of shape on green for button which is activated while entering level selection window
    if(gameState.formOfRotation == undefined || gameState.formOfRotation == "fluid")
        buttonContainer_fluentRotation.shadow       = new createjs.Shadow("green", 0, 0, 20);
    else
        buttonContainer_90_degrees.shadow           = new createjs.Shadow("green", 0, 0, 20);

    /**
     * function to create a button to distinct the form of rotation. This button has no functionality, is round
     * and has a Pentomino-brick as a label with an sprite defined in the input
     * @param {int} x x-coordinate of the button
     * @param {int} y y-coordinate of the button
     * @param {string} arrowSprite direction of the sprite as part of the label 
     * @param {int} rotation rotation-value of the arrowSprite
     * @param {*} arrowX x-coordinate of the arrowSprite
     * @param {*} arrowY y-coordinate of the button
     * @returns createjs.Container()-Object
     */
    function rotationButton(x, y, arrowSprite, rotation, arrowX, arrowY)
    {
        let buttonContainer                 = new createjs.Container();
        buttonContainer.x                   = x;
        buttonContainer.y                   = y;
        let sprite                          = new createjs.Bitmap(arrowSprite);
        sprite.rotation                     = rotation;
        sprite.scaleX                       = 0.1;
        sprite.scaleY                       = 0.1;
        sprite.alpha                        = 0.8;
        let shape                           = new createjs.Shape();
        setBrickForButton(20, descriptionNumber(require('../json/pentomino_brick_F.json').cells), shape);
        sprite.x                            = sprite.x + arrowX;
        sprite.y                            = sprite.y + arrowY;
        let circle_temp                     = new createjs.Graphics();
        circle_temp.beginFill("grey");
        circle_temp.drawCircle(0, 0, 70);
        circle                              = new createjs.Shape(circle_temp);
        circle.x                            += 30;
        circle.y                            += 30;
    
        buttonContainer.addChild( circle, shape, sprite );

        return buttonContainer;
    };

    /**
     * function to create a button to enter a playable level. This button has the functionality to enter the level
     * with the labeled Image as the playground by clicking on it. The style of the rotation is given as an attribut
     * with the gamestate, so the distinction which form of rotation to use can be used over several level instances.
     * @param {int} buttonWidth width of the button
     * @param {int} buttonHeight height of the button
     * @param {Pentomino_layerGrid} buttonIconLevel Pentomino_layerGrid used as the button label
     * @param {int} x x-coordinate of the button
     * @param {int} y y-coordinate of the button
     * @param {string} levelName name of the level, used to distinct, which level will be accessed by clicking 
     */
    function drawLevelButton(buttonWidth, buttonHeight, buttonIconLevel, x, y, levelName)
    {
        let levelButton = new createjs.Container();

        // sets the background of the button
        let buttonBackground = new createjs.Shape();
        buttonBackground.graphics.setStrokeStyle(3);
        buttonBackground.graphics.beginStroke("#000000");
        buttonBackground.graphics.beginFill("white");
        buttonBackground.graphics.drawRect( 0, 0,  buttonWidth * 1.4,  buttonHeight * 1.4 );
        buttonBackground.alpha = 0.5;
        levelButton.addChild(buttonBackground);

        // sets the image of the Pentomino_layerGrid from input as the label
        for(let i=0; i<buttonIconLevel.getLayerGrid_cells().length; i++)
            levelButton.addChild( buttonIconLevel.getLayerGrid_cells()[i] ),
        buttonIconLevel.getLayerGrid_cells()[i].x += 20,
        buttonIconLevel.getLayerGrid_cells()[i].y += 20;
        levelButton.x = x ;
        levelButton.y = y ;
    
        // event-listener for entering the level by clicking on the button
        levelButton.on("click", function()
        {
            stage.removeChild(settingBackgroundPicture);
            // gamestate gets the levelname and the style of the rotation as additionally attributs
            MTLG.lc.levelFinished( Object.assign(gameState, 
            {
                nextLevel: levelName,
                formOfRotation: formOfRotation
            }));
        });
        return levelButton;
    };

    /**
     * function to extend graphics to a createjs.Shape() object for creating a pentomino
     * @param {int} size size of the pentomino-cells
     * @param {Array} gridArray array, where the appearances of the pentomino-cells are coded by using "1" and "0" as characters
     * @param {createjs.Shape()} shape shape which will be extended
     */ 
    function setBrickForButton(size, gridArray, shape)
    {
        height         = gridArray.length;
        width          = gridArray[0].length;
        for( let i = 0; i < gridArray.length; i++)
        {
            for(let j = 0; j < gridArray[i].length; j++)
            {
                if(gridArray[i][j] == "1")
                {
                    shape.graphics.setStrokeStyle(2);
                    shape.graphics.beginStroke("#000000");
                    shape.graphics.beginFill("red");
                    shape.graphics.drawRect((j*size) , (i*size) , size, size);
                }
            }
        }
    };

    /**
     * function converts the input row to an array to draw the pentomino grid layer on the stage,
     * a "1" indicates the appearance of a cell in the grid, a "0" indicates the opposite.
     * @param {array} row coded input array
     */
    function descriptionNumber(row)
    {
        let row_array       = new Array();
        for(let j=0; j<row.length; j++)
        {
            let row_array_entry = new Array();
            row_array_entry = row[j].split(',');
            row_array.push(row_array_entry);
        }
        return row_array;
    };
}
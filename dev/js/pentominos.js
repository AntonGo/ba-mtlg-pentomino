/**
 * sets the pentomino bricks for the game Pentomino
 * @param {string} color color of the Pentomino
 * @param {int} size size of the pentomino cells
 * @param {createjs.Shape} shape shape for pentomino to set functionality for
 * @param {int} border border size of pentomino cells
 * @param {array} position_array array from affected json file, entries indicates how to draw the Pentomino brick,
 * a "1" means a cell should be drawn, a "0" means it shouldn´t.
 * @param {int} id number for identification of pentomino-brick
 * @param {array} gridCells array of grid cells from pentomino grid. Communication between Pentominos and 
 * the Field is done by the Pentomino_Cells, defined further below.
 */
var Set_Pentomino = function(color, size, shape, border, position_array, id, gridCells)
{
    // shape of the Pentomino-brick
    this.shape = shape;

    // array with the grid-cells of the grid-playground
    var array_gridCells             = new Array();
    array_gridCells                 = gridCells;

    // id of the pentomino-brick, id is the same as in the input id
    var ID                          = id;
    this.id                         = ID;

    // size of the pentomino-brick cells (here, width and height are the same)
    this.size                       = size;

    // converts the input position_array to an array to draw the pentomino-brick on the stage
    this.position_array             = position_array;
    let gridArray                   = descriptionNumber(this.position_array);

    // border-thickness of the pentomino-grid-cells
    this.border                     = border;

    // color of the pentomino-brick
    this.color                      = color;

    // width and height of the pentomino brick (calculated when brick is drawn on stage )
    var width;
    var height;

    // x- and y-Coordinate to set the pentomino on. The coordinates can only be changed by the
    // functions setPosX and setPosY below.
    var posX                        = 0;
    var posY                        = 0;

    // array where grid-cells are stored, which are covered by pentomio brick and are not already 
    // occupied by another brick
    var cover_array                 = new Array();

    // array where grid-cells are stored, which are covered by pentomio brick, cell can be occupied
    // already, this variable is used for handle case when a brick is placed either on another brick or
    // outside the grid
    var cellsTotal_array            = new Array();

    // coordinates of cell centers and actual cell-center createjs.Shape()-instances of petomino brick cells,
    // which are added in function createContainer in initialize-instance.
    let array_cellCenterCoordinates = new Array();
    let array_cellCenters           = new Array();

    // factor for tolerance in setting the pentomino-brick correctly in the field. 
    // A value near at size/2 means the setting shoud be very accurate, and a value near at 0 means less accuracy is possible.
    var factorForCollision          = size/8;

    // external setting of x-coordinate of Pentomino-brick. This is needed to set the x-coordinate of the container
    // resulting from this brick.
    this.setPosX = function(positionX)
    {
        posX = positionX;
    };

    // external setting of y-coordinate of Pentomino-brick. This is needed to set the y-coordinate of the container
    // resulting from this brick.
    this.setPosY = function(positionY)
    {
        posY = positionY;
    };

    // external accessing of x-coordinate of Pentomino-brick. This is needed to get the x-coordinate for the container
    // resulting from this brick.
    this.getPosX = function()
    {
        return posX;
    };

    // external accessing of y-coordinate of Pentomino-brick. This is needed to get the y-coordinate for the container
    // resulting from this brick.
    this.getPosY = function()
    {
        return posY;
    };

    // external setting of factorForCollision for Pentomino-brick. 
    this.setFactorForCollision = function(factor)
    {
        factorForCollision = factor;
    };

    // function for accessing the value for factorForCollision
    function getFactorForCollision()
    {
        return factorForCollision;
    };

    // flag if brick was put in wrong position, used to set x- and y-coordinate for next wrong positioning
    // on previous starting point before pressmove-event
    let flagForFalseSetting         = true;

    // x- and y-coordinate to put the brick on when pressup-action has failed
    let positionBeforeErrorX        = posX;
    let positionBeforeErrorY        = posY;

    // calculating correct window width and height due to possible changed window format,
    // factors are saved in fac1 (for width) and fac2 (for height)
    var levelOptions_1              = MTLG.getOptions();
    var height_1                    = levelOptions_1.height;
    var width_1                     = levelOptions_1.width;
    var fac1;
    var fac2;
    calculateFactorsForFormat(window.innerWidth, window.innerHeight, width_1, height_1);

    // draws the actual pentomino-brick on the stage
    setBrick(this.size, gridArray, shape);

    // the values for width and height are modified in the function "setBrick". For the real sizes of the Pentomino-brick,
    // this values are multiplied with the cellsizes.
    width             *= size;
    height            *= size;

    // adds for every grid-cell in pentomino grid an event-listener for pentomino-brick shape
    // (amount is necessary because hit-collision test is needed for every cell)
    for(let i = 0; i < array_gridCells.length; i++)
    {
        let shapes_temp_pressmove     = new createjs.Shape();
        shapes_temp_pressmove         = array_gridCells[i].shape;

        shape.on("pressmove", function(evt)
        {
            // if a previous error occured by positioning, the red shadow by this error is removed here
            if(cover_array.length == 5)
                shape.shadow = new createjs.Shadow("aqua", 0, 0, 50);
            else
                shape.shadow = new createjs.Shadow("red", 0, 0, 0);

            // when an error occured before, the backup-coordinates in this case will be set and flag will be updated
            if( getFlagForFalseSetting() )
                initialSetOfFlag();
            // occupied grid-cell will be available again when brick is lifted during pressmove-event
            releaseIfOccupiedByThisId(array_gridCells[i]);
            shapes_temp_pressmove.alpha = 0;

            // calculates position of current grid-cell in cover_array and cellsTotal_array, returns -1 if it isn´t
            idElementInArray = iDInArray(cover_array, array_gridCells[i]);
            idElementInArray_total = iDInArray(cellsTotal_array, array_gridCells[i]);

            // handles actions when pentomino-brick covers this grid-cell or not (for cover_array)
            if( detectCollision(shapes_temp_pressmove, size, shape, array_gridCells[i], true, getFactorForCollision()) )
            {
                // grid-cell adapts the color of the pentomino over it
                array_gridCells[i].colorize(color);
                shapes_temp_pressmove.alpha = 0.5;
                // grid-cell is pushed in cover_array when it´s not in there already
                if( idElementInArray == -1 )
                    cover_array.push(array_gridCells[i]); 
            }
            else
            {
                // grid-cell is removed from cover_array when it´s in there
                if( idElementInArray != -1 )
                    popElementFromArray(cover_array, idElementInArray);
            }   
            
            // handles actions when pentomino-brick covers this grid-cell or not (for cellsTotal_array)
            if( detectCollision(shapes_temp_pressmove, size, shape, array_gridCells[i], false, getFactorForCollision()) )
            { 
                // grid-cell is pushed in cellsTotal_array when it´s not in there already
                if( idElementInArray_total == -1 )
                    cellsTotal_array.push(array_gridCells[i]);
            }
            else
            {
                // grid-cell is removed from cellsTotal_array when it´s in there
                if( idElementInArray_total != -1 )
                    popElementFromArray(cellsTotal_array, idElementInArray_total);
            }             
        }); 
    }

    // event-listener for pressup-event of pentomino brick
    shape.on("pressup", handlePressupForPentomino);

    /**
     * function to handle pressup-event of pentomino brick
     * @param {event} evt event from pressup-event listener above
     */
    function handlePressupForPentomino(evt)
    {
        // sets id of grid-cells in cover_array on id of pentomino brick
        for(let i=0; i<cover_array.length; i++)
        {
            let index = cover_array[i].getCellID();
            if(gridCells[index] != undefined)
                gridCells[index].setSetFor(ID);
        };

        // if pentomino-brick is put entirely in grid, automatically setting of brick will be triggered,
        // if brick is not put correctly, the brick will be put apart of actual position with red shadow
        if(cover_array.length == 5 )
        {
            shape.shadow = new createjs.Shadow("red", 0, 0, 0);
            document.dispatchEvent( new CustomEvent("Pentomino_rotate", { "detail": ID, cancelable: true }) );

            // calculates correct position for pentomino brick depending of window format
            let modified_array_pent = calculatePentominoCellCenters(array_cellCenters);
            let res = calculatePointForSet(cover_array, modified_array_pent);

            // fires an event to Movable-instance of this brick to set brick in the right position
            document.dispatchEvent( new CustomEvent("Pentomino_moveTo", { "detail": [evt.stageX - (res[0])*fac1, evt.stageY - (res[1])*fac2, ID ], cancelable: true }));
        }
        else if((cover_array.length < 5 && cover_array.length > 0) || (cover_array.length != cellsTotal_array.length))
        {
            // restore default attributes alpha of shape and setFor of grid-cell
            for(let i=0; i<cover_array.length; i++)
                cover_array[i].shape.alpha = 0, cover_array[i].setSetFor(null);
            shape.shadow = new createjs.Shadow("red", 0, 0, 50);

            setFlagForFalseSetting(true);
            // fires an event to Movable-instance of this brick to set brick in the intended position
            document.dispatchEvent( new CustomEvent("Pentomino_moveTo", { "detail": [positionBeforeErrorX, positionBeforeErrorY, ID ], cancelable: true }));
        }
    };

    /**
     * function calculates the pentomino-cell centers of input array depending of the actual window format
     */
    function calculatePentominoCellCenters(array_pent)
    {
        let temp_array = new Array();
        for(let i=0; i<array_pent.length; i++)
        {
            let point = array_pent[i].localToGlobal(array_pent[i].x, array_pent[i].y);
            let point2 = MTLG.getStageContainer().globalToLocal(point.x, point.y);
            temp_array.push(point2);
        }
        return temp_array;
    };

    /**
     * function calculates the point to set the pentomino brick when pressup is successful, function calculates
     * point where an element of array_grid has the shortest distance to an element of array_pent and calculates
     * the right shift directions
     * @param {createjs.Array} array_grid array of grid-cells
     * @param {createjs.Array} array_pent array of pentomino brick cells
     * return an array with two entries: x-distance and y-distance
     */
    function calculatePointForSet(array_grid, array_pent)
    {
        let result          = new Array();
        let temp            = Number.MAX_SAFE_INTEGER;
        let grid_cell       = -1;
        let pent_cell       = -1;

        for(let i=0; i< array_grid.length; i++)
        {
            for(let j=0; j< array_pent.length; j++)
            {
                let distance = distanceBetweenPoints(array_grid[i].getCenterX(), array_grid[i].getCenterY(), array_pent[j].x, array_pent[j].y);
                if(distance < temp)
                {
                    grid_cell = i, pent_cell = j;
                    temp = distance;
                }    
            }    
        }
        result.push(array_pent[pent_cell].x - array_grid[grid_cell].getCenterX(), array_pent[pent_cell].y - array_grid[grid_cell].getCenterY());  
        return result;
    };

    /**
     * function draws the pentomino brick as a shape, furthermore the cell-centers of the brick cells are stored here
     * @param {int} size size of the pentomino cells
     * @param {createjs.Array} gridArray two-dimensional array where there is marked if a cell should be drawn or not
     * @param {createjs.Shape} shape shape where the pentomino-cells will be added on
     */
    function setBrick(size, gridArray, shape)
    {
        height         = gridArray.length;
        width          = gridArray[0].length;
        for( let i = 0; i < gridArray.length; i++)
        {
            for(let j = 0; j < gridArray[i].length; j++)
            {
                if(gridArray[i][j] == "1")
                {
                    let cellCenterCordinates = new Array();
                    shape.graphics.setStrokeStyle(border);
                    shape.graphics.beginStroke("#000000");
                    shape.graphics.beginFill(color);
                    shape.graphics.drawRect((j*size) , (i*size) , size, size);
                    cellCenterCordinates.push((j*size)+(size/2), (i*size)+(size/2));
                    array_cellCenterCoordinates.push(cellCenterCordinates);
                }
            }
        }
    };

    /**
     * function: occupied grid-cell will be available again when brick is lifted in pressmove-event
     * @param {Pentomino_Cell} cell 
     */
    function releaseIfOccupiedByThisId(cell)
    {
        if( cell.getSetFor() == ID )
            cell.setSetFor(null);
    };

    /**
     * grid-cell is removed from cover_array, positionInArray marks the position in the cover_array
     */
    function popElementFromArray(cover_array, positionInArray)
    {
        let temp                        = cover_array[cover_array.length-1];
        cover_array[positionInArray]    = temp;
        cover_array.pop();
    };

    /**
     * function for getting variable cover_array for external use
     */
    this.getCover_array = function()
    {
        let temp    = new Array();
        temp        = cover_array;
        return temp;
    };

    /**
     * function sets flagForFalseSetting on input "bool"
     */
    function setFlagForFalseSetting(bool)
    {
        flagForFalseSetting = bool;
    };

    /**
     * function for accessing "flagForFalseSetting"
     */
    function getFlagForFalseSetting()
    {
        return flagForFalseSetting;
    };

    /**
     * sets the backup-coordinates in this case will be set by event details and flag flagForFalseSetting 
     * will be updated
     * @param {Event} evt event for positioning
     */
    function initialSetOfFlag()
    {
        setFlagForFalseSetting(false);
        positionBeforeErrorX = posX * fac1;
        positionBeforeErrorY = posY * fac2;
    };

    /**
     * function get-function for array array_cellCenters
     */
    this.getArray_cellCenters = function()
    {
        return array_cellCenters;
    };

    /**
     * function for pushing an element in array_cellCenters
     * @param {createjs.Shape} element
     */
    this.pushArray_cellCenters = function(element)
    {
        array_cellCenters.push(element);
    };

    /**
     * function get-function for array array_cellCenterCoordinates
     */
    this.getArray_cellCenterCoordinates = function()
    {
        return array_cellCenterCoordinates;
    };

    /**
     * function get-function for this.id
     */
    function getID()
    {
        return this.id;
    };

    /**
     * function for external setting of variable "id"
    */
    this.setID = function(id)
    {
        ID = id;
    };

    /**
     * function get-function for pentomino-brick "shape"
     */
    this.getShape = function()
    {
        return this.shape;  
    };

    /**
     * function get-function for pentomino brick width
     */
    this.getWidth = function()
    {
        return width;
    };

    /**
     * function get-function for pentomino brick height
     */
    this.getHeight = function()
    {
        return height;
    };

    /**
     * function get-function for factor fac1 calculated in pentomino brick instance
     */
    this.getFac1 = function()
    {
        return fac1;
    };

    /**
     * function get-function for factor fac2 calculated in pentomino brick instance
     */
    this.getFac2 = function()
    {
        return fac2;
    };

    /**
     * function decides if the pentomino brick is set in the pentomino grid correctfully
     * @param {createjs.Shape} shapes_temp shape of the grid cell
     * @param {int} size size of the pentomino grid cell (width = height is suggested)
     * @param {createjs.Shape} shape shape of the pentomino brick
     * @param {Pentomino_Cell} array_gridCell grid cell
     * @param {Boolean} brickOverlapping boolean to decide if grid cell is pushed in cover array (when
     * brickOverlapping is true) and/or pushed in cellsTotal_array (when brickOverlapping is false)
     * @param {double} factor value for setting the accuracy for the pressup-event for the pentomino bricks.
     */
    function detectCollision(shapes_temp, size, shape, array_gridCell, brickOverlapping, factor)
    {
        let pt_1                = shapes_temp.localToLocal(size/2 - factor, size/2 - factor, shape);
        let pt_2                = shapes_temp.localToLocal(size/2 + factor, size/2 + factor, shape); 
        let pt_3                = shapes_temp.localToLocal(size/2 - factor, size/2 + factor, shape); 
        let pt_4                = shapes_temp.localToLocal(size/2 + factor, size/2 - factor, shape); 
        let hit                 = shape.hitTest(pt_1.x, pt_1.y) && shape.hitTest(pt_2.x, pt_2.y) && shape.hitTest(pt_3.x, pt_3.y) && shape.hitTest(pt_4.x, pt_4.y);
        if(brickOverlapping)
            return hit && ( array_gridCell.getSetFor() == null );
        else
            return hit;
    };

    /**
     * function gets an array and a pentomino cell as input and returns the index of the array if the element is 
     * in it depending on their id´s, or -1 if the element is not in the array 
     * @param {Array} array 
     * @param {Pentomino_Cell} array_element 
     */
    function iDInArray(array, array_element)
    {
        let index               = -1;
        let multipleTimes       = 0;
        for(let i=0; i<array.length; i++)
        {
            if(array[i].getCellID() == array_element.getCellID() && multipleTimes > 0)
                index           = -1;
            else if(array[i].getCellID() == array_element.getCellID() && multipleTimes == 0)
                index           = i, multipleTimes++;  
        }
        return index;
    };

    // MTLG standard window format is 16/9, handle possible different window formats:
    function calculateFactorsForFormat(innerWidth, innerHeight, width_1, height_1)
    {
        fac1 = innerWidth/width_1;
        fac2 = innerHeight/height_1;
        if(innerWidth/innerHeight > 16/9)
        {
            fac1 = fac2;
        }
        else if(innerWidth/innerHeight < 16/9)
        {
            fac2 = fac1;
        }
    };

    /**
     * function calculates the distance between points (x1,y1) and (x2,y2)
     */
    function distanceBetweenPoints(x1, y1, x2, y2) 
    {
        // Pythagorean theorem
        let deltaX  = Math.abs(x1 - x2);
        let deltaY  = Math.abs(y1 - y2);
        dist        = Math.sqrt(Math.pow(deltaX, 2) + Math.pow(deltaY, 2));
        return dist;
    };
};

/**
 * constructor of the pentomino grid with all functionality needed
 * @param {int} size size of the grid cells
 * @param {Array} position_array coded array from the concerned json file where it is decided,
 * if a grid cell should be drawn on the stage
 * @param {int} posX x-coordinate where the pentomino grid should be set
 * @param {int} posY y-coordinate where the pentomino grid should be set
 */
var Pentomino_Grid = function(size, position_array, posX, posY)
{
    // indicator for winning the level, count starts at the beginning of the level with the amount 
    // of grid-cells of the pentomino grid and the winning condition is triggered, when count reaches 0
    var amountFreeCells = new amountFreeCells();
    this.amountFreeCells = amountFreeCells;
    // array where grid cells are stored
    var array_gridCells = new Array();
    var stage           = MTLG.getStageContainer();
    // variable for setting the ID´s of the grid cells
    let ID              = 0;
    
    // draws the pentomino grid on the stage depending on coding in array position_array
    let gridArray = descriptionNumber(position_array);
    setGrid(size, gridArray);

    /**
     * function draws grid on stage depending on coding in array gridArray
     * @param {int} size size of pentomino cells
     * @param {Array} gridArray coded grid in an array
     */
    function setGrid(size, gridArray)
    {
        for( let i = 0; i < gridArray.length; i++)
        {
            for(let j = 0; j < gridArray[i].length; j++)
            {
                if(gridArray[i][j] == "1")
                {
                    let shape       =  new createjs.Shape();
                    shape.graphics.setStrokeStyle(2);
                    shape.graphics.beginStroke("#000000");
                    shape.graphics.drawRect(0 , 0 , size, size);
                    shape.x = posX+((j)*size);
                    shape.y = posY+(i*size);
                    shape.regX      = size/2;
                    shape.regY      = size/2;
                    // makes a separate Pentomino_Cell-Object for every grid-cell
                    let cell        = new Pentomino_Cell(shape, ID, size);
                    array_gridCells.push(cell);
                    stage.addChild(shape); 
                    ID++;
                    // before the game starts, amountFreeCells.number as the indicator for the winning-condition
                    // is set to the amount of grid-cells in the playground
                    amountFreeCells.number++;
                }
            }
        }
    };

    /**
     * function for accessing cells of pentomino grid for external use
     */
    this.getArray_gridCells = function()
    {
        return array_gridCells;
    }

    /**
     * Event-listener for handling custom increaseCellCount-event of pentomino-brick,
     * increases the variable amountFreeCells.
     */    
    document.addEventListener("increaseCellCount", increaseCellCount);

    function increaseCellCount(evt)
    {
        if(Number.isInteger(amountFreeCells.number))
        {
            amountFreeCells.number++;
            console.log(amountFreeCells.number);
        }
        evt.preventDefault(); 
    };

    /**
     * Event-listener for handling custom decreaseCellCount-event of pentomino-brick,
     * decreases the variable amountFreeCells and tests if win-condition is reached
     */  
    document.addEventListener("decreaseCellCount", decreaseCellCount);
    
    function decreaseCellCount(evt)
    {
        if(Number.isInteger(amountFreeCells.number))
        {
            amountFreeCells.number--;
            console.log(amountFreeCells.number);
            win_condition();
        }
        evt.preventDefault(); 
    };

    /**
     * function tests if win-condition (amountFreeCells == 0) is reached, when it is reached,
     * an event with name "win-condition" is fired to handle "winning the level"
     */
    function win_condition()
    {
        if(amountFreeCells.number == 0)
        {
            document.dispatchEvent( new CustomEvent("win-condition", { "detail": "", cancelable: true }));
            delete amountFreeCells.number;
        }
    };

    // Constructor of Object "amountFreeCells" with only attribute "number". It is necessary to make 
    // restarting the level possible after first time winning. 
    function amountFreeCells() {
        this.number = 0;
    }

    /**
     * function to delete the value "amountFreeCells.number" after leaving the level and setting the
     * setFor-values of all grid-cells to "-1" when leaving the level without winning.
     * This is necessary to start a level by re-entering only with the visible grid-cells.
     */
    function deleteAmountFreeCells()
    {
        console.log("amountFreeCells.number: "+amountFreeCells.number);
        delete amountFreeCells.number;
        console.log("amountFreeCells.number: "+amountFreeCells.number);
        for(let i=0; i < array_gridCells.length; i++)
        {
            array_gridCells[i].setSetForForce(-1);
        };
    };
    
    // function for external use to delete the "amountFreeCells.number"-value
    this.deleteAmountFreeCells = function()
    {
        deleteAmountFreeCells();
    };

};
   
/**
 * constructor of Pentomino_Cell
 * @param {createjs.Shape} shape shape to be extended in this constructor
 * @param {int} ID identification number of this pentomino cell
 * @param {int} size size of pentomino cell
 */
var Pentomino_Cell = function(shape, ID, size)
{
    this.shape          = shape;

    // variable setFor is used to assign the grid cell to a pentomino brick by its "ID"
    var setFor          = null;
    this.size           = size;

    // center coordinates of pentomino cell
    let centerX         = shape.x; 
    let centerY         = shape.y; 

    /**
     * function for external use to colorize the grid cell by input color when a pentomino brick
     * is hovered over it
     * @param {string} color color which the cell is colorized
     */
    this.colorize       = function(color)
    {
        let fill = new createjs.Graphics.Fill(color);
        shape.graphics.append(fill);
    };

    /**
     * function for external use to get the x-coordinate of the cell center 
     */
    this.getCenterX = function()
    {
        return centerX;
    };

    /**
     * function for external use to get the y-coordinate of the cell center 
     */
    this.getCenterY = function()
    {
        return centerY;
    };

    /**
     * function for external use to get the setFor attribute of the cell
     */
    this.getSetFor      = function()
    {
        return setFor;
    };

    /**
     * function for external use to set the setFor attribute of the cell, here the amountFreeCells
     * variable of the pentomino grid is increased/decreased by firing custom events in involved functions
     */
    this.setSetFor      = function(pent)
    {
        if(setFor == null) // cell is not set by a pentomino, but is in the process of it
        {
            if(pent != null)
            {
                setFor = pent;
                decreaseCellCount();
            }  
        }
        else // cell is set by a pentomino, but is in the process of releasing it
        {
            if(pent == null)
            {
                setFor = null;
                increaseCellCount();
            }
        }
    };

    // function for external use to set a setFor-attribut of a cell to "-1" without updating an amountFreeCells.number value
    this.setSetForForce = function(value)
    {
        setFor = value;
    };

    /**
     * function for external use to get the cell ID of the cell
     */
    this.getCellID      = function()
    {
        return ID;
    };

    /**
     * function firing custom event "increaseCellCount" for increasing attribute amountFreeCells of Pentomino grid
     */
    function increaseCellCount()
    {
        document.dispatchEvent( new CustomEvent("increaseCellCount", { "detail": "", cancelable: true }));
    };
    
    /**
     * function firing custom event "decreaseCellCount" for decreasing attribute amountFreeCells of Pentomino grid
     */
    function decreaseCellCount()
    {
        document.dispatchEvent( new CustomEvent("decreaseCellCount", { "detail": "", cancelable: true }));
    };
};

/**
 * constructor of the pentomino grid layer behind the actual pentomino grid (without any functionality for playing, only for visual appearance)
 * @param {int} size size of the grid cells
 * @param {Array} position_array coded array from the concerned json file where it is decided,
 * if a grid cell should be drawn on the stage
 * @param {int} posX x-coordinate where the pentomino grid should be set
 * @param {int} posY y-coordinate where the pentomino grid should be set
 */
var Pentomino_layerGrid = function(size, position_array, posX, posY, isMenu)
{
    var stage               = MTLG.getStageContainer();
    var layerGrid_cells     = new Array();
    var ux                  =   0;
    var uy                  =   0;
    var dx                  =   0;
    var dy                  =   0;

    
    // draws the grid by an coded array from a json-file, like the Pentomino-bricks and the Pentomino-Grid
    let gridArray = descriptionNumber(position_array);
    setGrid(size, gridArray);

    function setGrid(size, gridArray)
    {
        for( let i = 0; i < gridArray.length; i++)
        {
            for(let j = 0; j < gridArray[i].length; j++)
            {
                if(gridArray[i][j] == "1")
                {
                    let shape       = new createjs.Shape();
                    shape.graphics.setStrokeStyle(2);
                    shape.graphics.beginStroke("#000000");
                    shape.graphics.beginFill("lightgrey");
                    shape.graphics.drawRect(0 , 0 , size, size);
                    shape.x = posX+((j)*size);  shape.y = posY+(i*size);
                    if(!isMenu)
                        shape.regX      = size/2,   shape.regY      = size/2;
                    layerGrid_cells.push(shape);
                    if(!isMenu)
                        stage.addChild(shape); 
                    // sets variables for later calculating of the width and height of this grid
                    if(shape.x < ux )
                        ux = shape.x;
                    if(shape.y < uy)
                        uy = shape.y;
                    if( (shape.x + size) > dx)
                        dx = (shape.x + size);
                    if( (shape.y + size) > dy)
                        dy = (shape.y + size);
                }
            }
        }
    };

    // function for external accessing of the layer grid-cells by an array
    this.getLayerGrid_cells = function()
    {
        return layerGrid_cells;
    };

    // width- and height value of the Pentomino-Grid layer calculated from previous functions
    this.width              = dx - ux;
    this.height             = dy - uy;

}

/**
 * function converts the input row to an array to draw the pentomino grid layer on the stage,
 * a "1" indicates the appearance of a cell in the grid, a "0" indicates the opposite.
 * @param {array} row coded input array
 */
function descriptionNumber(row)
{
    let row_array       = new Array();
    for(let j=0; j<row.length; j++)
    {
        let row_array_entry = new Array();
        row_array_entry = row[j].split(',');
        row_array.push(row_array_entry);
    }
    return row_array;
};

/**
 * function constructor to render the grids, pentomino bricks and adding  functionality for 
 * connecting them on the stage
 * @param {array} array array of letters to code which pentomino bricks should be added to the stage
 * @param {int} steps parameter to decide in which steps the pentomino bricks should be rotated, 0 means smooth rotating
 * @param {int} param1 parameter to decide which mode of handling rotating, moving and reflecting should be used, here it only exists
 * one mode, but can be extended by own methods
 * @param {JSON} pentomino_grid1 data coded in json to build pentomino grids
 * @param {int} cellSize size of pentomino celld (bricks and grids)
 * @param {int} xCor x-coordinate for the grids
 * @param {int} yCor y-coordinate for the grids
 * @param {int} jumps factor to multiply to rotation of Pentomino-brick
 */
var initialize = function(array, steps, param1, pentomino_grid1, cellSize, xCor, yCor, jumps)
{
    // variable to draw pentomino brick on stage
    let requirement;

    // initialization of all instances
    var stage           = MTLG.getStageContainer();
    var list_Movables   = new SetOfMovables({steps: steps, param1: param1, jumps: jumps});
    var list_Pentominos = new Array();
    var testGrid_layer  = new Pentomino_layerGrid(cellSize, pentomino_grid1.cells, xCor, yCor, false);
    var testGrid        = new Pentomino_Grid(cellSize, pentomino_grid1.cells, xCor, yCor);

    // decides which pentomino brick should be selected for every entry in input "array", and creating
    // a container and a Movable-Object of every one of it
    for(let i=0; i<array.length; i++)
    {
        let color;
        switch(array[i]) {
            case "f":
                requirement = require('../json/pentomino_brick_F.json');
                color = "red";
                break;
            case "i":
                requirement = require('../json/pentomino_brick_I.json');
                color = "blue";
                break;
            case "l":
                requirement = require('../json/pentomino_brick_L.json');
                color = "green";
                break;
            case "p":
                requirement = require('../json/pentomino_brick_P.json');
                color = "yellow";
                break;
            case "n":
                requirement = require('../json/pentomino_brick_N.json');
                color = "orange";
                break;
            case "t":
                requirement = require('../json/pentomino_brick_T.json');
                color = "purple";
                break;
            case "u":
                requirement = require('../json/pentomino_brick_U.json');
                color = "brown";
                break;
            case "v":
                requirement = require('../json/pentomino_brick_V.json');
                color = "turquoise";
                break;
            case "w":
                requirement = require('../json/pentomino_brick_W.json');
                color = "violet";
                break;
            case "x":
                requirement = require('../json/pentomino_brick_X.json');
                color = "steelblue";
                break;
            case "y":
                requirement = require('../json/pentomino_brick_Y.json');
                color = "yellowgreen";
                break;
            case "z":
                requirement = require('../json/pentomino_brick_Z.json');
                color = "wheat";
                break;
            default: 
        }
        
        // creating every Set_Pentomino-Object necessary in this level and stores it in an array
        let shape       = new createjs.Shape();
        let pent        = new Set_Pentomino(color, cellSize, shape, 2, requirement.cells, requirement.id[0], testGrid.getArray_gridCells());
        pent.setFactorForCollision(cellSize*(3/12));
        list_Pentominos.push(pent);
        setCoordinatesOfPentominos(list_Pentominos);

        // creating a Container out of every Set_Pentomino-Object
        let container1  = createContainer(pent);

        // creating a Movable-Object out of every created Container from the previous step,
        // then adding them to a SetOfMovables-Object
        let move_cont   = new Movable(container1, pent.getWidth()/2, pent.getHeight()/2, "Container" );
        move_cont.setID(requirement.id[0]);
        stage.addChild( move_cont.shape );
        list_Movables.addToMovables(move_cont);

    };

    // event-listener for the event "Pentomino_rotate", where the Pentomino-brick, in which the event was
    // fired, sets his rotation-attribut as described in the involved function
    document.addEventListener("Pentomino_rotate", externalRotation);

    function externalRotation(evt)
    {
        for(let i=0; i<list_Movables.lengthSize(); i++)
            if(list_Movables.setOfMovables[i].id == evt.detail)
                list_Movables.setOfMovables[i].setExternalRotation(90);
        evt.preventDefault();
    };

    // event-listener for the event "Pentomino_moveTo", where the Pentomino-brick, in which the event was
    // fired, sets his x- and y-coordinate as described in the involved function
    document.addEventListener("Pentomino_moveTo", externalMoveTo);

    function externalMoveTo(evt)
    {
        for(let i=0; i<list_Movables.lengthSize(); i++)
            if(list_Movables.setOfMovables[i].id == evt.detail[2])
                list_Movables.setOfMovables[i].moveFormTo(evt.detail[0], evt.detail[1]);  
        evt.preventDefault();       
    };

    /**
     * function to create a container with pentomino shapes and shapes on every brick cell centers
     * @param {Set_Pentomino} pentomino 
     */
    function createContainer(pentomino)
    {
        let container1 = new createjs.Container();
        container1.addChild(pentomino.getShape());
        container1.setBounds(0, 0, pentomino.getWidth(), pentomino.getHeight());
        container1.x = pentomino.getPosX();
        container1.y = pentomino.getPosY();
        for(let i=0; i<pentomino.getArray_cellCenterCoordinates().length; i++)
        {
            let shape = new createjs.Shape();
            shape.graphics.setStrokeStyle(2);
            shape.graphics.beginStroke("#000000");
            shape.graphics.beginFill("red");
            shape.graphics.x = pentomino.getArray_cellCenterCoordinates()[i][0] * (1/2); 
            shape.graphics.y = pentomino.getArray_cellCenterCoordinates()[i][1] * (1/2);
            shape.name = "pentomino_cell";
            shape.x = shape.graphics.x;
            shape.y = shape.graphics.y;
            // shape.graphics.drawRect(shape.x, shape.y , 2, 2);
            container1.addChild(shape);
            pentomino.pushArray_cellCenters(shape);
        }
        return container1;
    };

    /**
     * function clears elements added to stage after reaching the win-condition or byleaving the level early
     */
    this.clearListMovables = function()
    {
        for(let i=0; i<list_Movables.setOfMovables.length; i++)
            stage.removeChild(list_Movables.setOfMovables[i].shape);

        for(let i=0; i<testGrid_layer.getLayerGrid_cells().length; i++)
            stage.removeChild(testGrid_layer.getLayerGrid_cells()[i]);

        delete testGrid.amountFreeCells;
    };

    // functions for external removing of all event-listeners from all Movable- and Pentomino-brick-Objects
    // in this stage
    this.removeAllEventListenersFromMovable = function()
    {
        for(let i=0; i<list_Movables.setOfMovables.length; i++)
            list_Movables.setOfMovables[i].setAllowToMove(false);

        for(let i=0; i<list_Pentominos.length; i++)
            list_Pentominos[i].getShape().removeAllEventListeners();
    };

    // Sets the x- and y-coordinate of all Set_Pentomino-Objects to a specified value.
    // This is possible up to 20 Set_Pentomino-Objects. For more objects, the default coordinates
    // will be used.
    function setCoordinatesOfPentominos(list_Pentominos)
    {
        let options_x = MTLG.getOptions().width/16;
        let options_y = MTLG.getOptions().height/9;
        let coordinates = new Array();
        coordinates.push(   [options_x*2, options_y*1.5], [options_x*14, options_y*7.5],
                            [options_x*14, options_y*1.5], [options_x*2, options_y*7.5],

                            [options_x*12, options_y*8], [options_x*4, options_y],
                            [options_x*4, options_y*8], [options_x*12, options_y],

                            [options_x*15, options_y*4.5], [options_x*1, options_y*4.5],
                            [options_x*8, options_y], [options_x*8, options_y*8],

                            [options_x, options_y*3], [options_x*15, options_y*6],
                            [options_x*15, options_y*3], [options_x, options_y*6],

                            [options_x*6, options_y], [options_x*10, options_y*8],
                            [options_x*10, options_y], [options_x*6, options_y*8]);
        for(let i=0; i<coordinates.length; i++)
            if(list_Pentominos[i] != undefined)
                list_Pentominos[i].setPosX(coordinates[i][0]),
                list_Pentominos[i].setPosY(coordinates[i][1]);
    };

    // function for external deletion of the amountFreeCells.number-value of the involved grid and setting
    // the setSetFor-attribut of all Pentomino_Cell-Objects in this level to "-1"
    this.deleteFreeCellsAfterAbortion = function()
    {
        testGrid.deleteAmountFreeCells();
    };
};
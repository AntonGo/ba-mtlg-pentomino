
function drawMainMenu(){

  // log in demo player if not yet present
  if (!MTLG.getPlayerNumber()) {
      console.log("Logging in");
      MTLG.lc.goToLogin(); //Leave Main Menu and go to login
  }

  MTLG.setBackgroundColor('white');
  
  MTLG.clearStage();
  MTLG.lc.levelFinished({
    nextLevel: "levelSelection"
  });
}
